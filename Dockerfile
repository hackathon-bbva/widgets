FROM registry.gitlab.com/hackathon-bbva/dashr:latest

LABEL maintainer "Fabian Santander <hdmsantander@gmail.com>"

ENV DATABASE_HOST localhost
ENV DATABASE_USER datastream
ENV DATABASE_PASSWORD datastream
ENV DATABASE_NAME datastream
ENV DATABASE_PORT 5432

RUN R -e "version"

WORKDIR /srv/backend

COPY ./backend/ /srv/backend

EXPOSE 8050

CMD ["/usr/local/bin/R","-e source('/srv/backend/index.R')"]
