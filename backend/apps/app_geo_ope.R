library(dashHtmlComponents)
library(dashCoreComponents)
library(plotly)
library(dashTable)
library(rjson)

serve_geo_ope_layout <- function(anio="2020",mes="05"){
  
  ope_data <- readRDS(paste0("./data/BM_Operativa_",anio,mes,".rds"))
  ope_data <- get_from_db_by_name_year_month_type(paste0("BM_Operativa_",anio,mes),as.numeric(anio),as.numeric(mes),1)
  
  date_range <- get_date_range(tipo_archivo = "Operativa")
  
  # App layout ####
  
  prod_opts_ope <- lapply(1:length(ope_data$catalogo_prod),
                          function(i){
                            list(label = paste(ope_data$catalogo_prod[i],":",names(ope_data$catalogo_prod[i])),
                                 value = unname(ope_data$catalogo_prod[i]))
                          })
  banc_opts_ope <- lapply(1:length(ope_data$catalogo_banc),
                          function(i){
                            list(label = paste(ope_data$catalogo_banc[i],":",names(ope_data$catalogo_banc[i])),
                                 value = unname(ope_data$catalogo_banc[i]))
                          })
  arit_opts_ope <- list(
    list(label = "+",value = "+"),
    list(label = "-", value = "-"),
    list(label = "*", value = "*"),
    list(label = "/", value = "/")
  )
  
  tabla_data_ope <- dashDataTable(
    id = "data_table_1",
    style_table= list(
      maxHeight = '300px',
      overflowY = 'scroll'
    ),
    export_columns = "all",
    export_format = "csv",
    columns = list(
      list(id = "AGEE", name = "Codigo"),
      list(id = "VALOR", name = "Valor"),
      list(id = "NOMBRE", name = "Estado")
    )
  )
  
  app_geo_ope_layout <-  htmlDiv(children = list(
    htmlDiv(children = list(
      htmlDiv(children = list(
        htmlLabel('Cifra'),
        dccRadioItems(
          id = "cifra_radio_ope",
          options = list(list(label = "Saldo de producto", value = "sal_prod"),
                         list(label = "Número total", value = "num_total")),
          value = 'num_total'
        ),
        htmlBr(),
        htmlLabel("Bancos en México"),
        dccDropdown(
          id = "banc-d_ope_1",
          options = banc_opts_ope,
          value = unname(ope_data$catalogo_banc[1]),
          clearable = FALSE
        ),
        htmlLabel("Cifra operativa"),
        htmlDiv(children = list(
          htmlDiv(
            dccDropdown(
              id = "prod-d_ope_1",
              clearable = FALSE,
              options = prod_opts_ope,
              value = unname(ope_data$catalogo_prod[1]),
              optionHeight = 68
            ),
            style = list(width = "25%", display = "inline-block",
                         "font-size"="small")
          ),
          htmlDiv(
            dccDropdown(
              id = "arit-d_ope_1",
              clearable = FALSE,
              options = arit_opts_ope,
              value = "+",
            ),
            style = list(width = "8%", display = "inline-block",
                         "font-size"="small")
          ),
          htmlDiv(
            dccDropdown(
              id = "prod-d_ope_2",
              clearable = TRUE,
              options = prod_opts_ope,
              value = NULL,
              placeholder = "Seleccione una opción",
              optionHeight = 68
            ),
            style = list(width = "25%", display = "inline-block",
                         "font-size"="small")
          ),
          htmlDiv(
            dccDropdown(
              id = "arit-d_ope_2",
              clearable = FALSE,
              options = arit_opts_ope,
              value = "+",
            ),
            style = list(width = "8%", display = "inline-block",
                         "font-size"="small")
          ),
          htmlDiv(
            dccDropdown(
              id = "prod-d_ope_3",
              clearable = TRUE,
              options = prod_opts_ope,
              value = NULL,
              placeholder = "Seleccione una opción",
              optionHeight = 68
            ),
            style = list(width = "25%", display = "inline-block",
                         "font-size"="small")
          )
        )
        ),
        htmlLabel("Comparación temporal"),
        htmlDiv(
          htmlDiv(children=list(
            dccDatePickerSingle(
              id='picker_fecha_ope',
              display_format = "Y MMMM",
              min_date_allowed=min(date_range),
              max_date_allowed=as.Date(paste(anio,mes,"01",paste="-"),format = "%Y-%m-%d"),
              initial_visible_month=min(date_range)
            ),
            htmlButton("GO", id="sb_time_ope",style = list("margin-left"="2em"))
          )
          ),
          style = list(display="inline-block","margin-left"="3em")
        )
        ,
        htmlBr(),
        tabla_data_ope
      ),
      style = list(width = "50%", display = "inline-block",
                   height = "480px", "vertical-align" = "top")
      ),
      htmlDiv(
        dccGraph(
          id = 'geo_mex_graph_1',
          style = list(width='100%',padding="0px 0px 0px 0px", margin = "0px 0px")
        ),
        style = list(width = "50%", display = "inline-block",
                     height = "480px", "padding-top" = "0px", "vertical-align" = "top")
      )
    ),
    style = list("background-color" = colors$background)
    ),
    htmlBr(),
    htmlBr(),
    htmlDiv(children = list(
      dccInput(id="in-box_ope", type="text"),
      htmlButton("Submit", id="sb_button_ope"),
      htmlDiv(id="msg_mac_sb_ope",
              children="Introduzca una formula y presione el botón")
    ),
    hidden = TRUE
    )
  )
  )
  
  return(app_geo_ope_layout)
  
}


# App callback ####


app$callback(
  output(id = "data_table_1", property = "value"),
  params = list(
    input(id = "geo_mex_graph_1", property = "selectedData")
  ),
  function(selectedData){
    if(is.null(selectedData$points)){
      return(dashNoUpdate())
    } else {
      return(dashNoUpdate())
    }
  }
)


app$callback(
  output = list(
    output(id = "geo_mex_graph_1", property = "figure"),
    output(id = "data_table_1", property = "data")
  ),
  params = list(input(id = "prod-d_ope_1", property = "value"),
                input(id = "banc-d_ope_1", property = "value"),
                input(id = "cifra_radio_ope", property = "value"),
                input(id = "arit-d_ope_1", property = "value"),
                input(id = "prod-d_ope_2", property = "value"),
                input(id = "arit-d_ope_2", property = "value"),
                input(id = "prod-d_ope_3", property = "value"),
                input(id = "sb_time_ope", property = "n_clicks"),
                state(id = "picker_fecha_ope", property = "date"),
                state(id = "url", property = "search")),
  
  function(producto1, banco_principal, cifra, arit1, producto2, arit2,producto3,t_nclicks,date_target,search_par){
    
    params <- unlist(strsplit(search_par,"?",fixed = TRUE))
    params <- params[2:length(params)]
    pos_mes <- grep("mes",params)
    pos_anio <- grep("anio",params)
    anio <- strsplit(params,"=")[[pos_anio]][2]
    mes <- strsplit(params,"=")[[pos_mes]][2]
  ope_data <- get_from_db_by_name_year_month_type(paste0("BM_Operativa_",anio,mes),as.numeric(anio),as.numeric(mes),1)
    
    
    
    with(ope_data,{
      parsed_command <- producto1
      parsed_title <- names(catalogo_prod)[match(unlist(producto1),catalogo_prod)]
      if(!is.null(unlist(producto2))){
        parsed_command <- paste("(",producto1,arit1,producto2,")")
        parsed_title <- paste("(",parsed_title,arit1,"<br>",
                              names(catalogo_prod)[match(unlist(producto2),catalogo_prod)],")")
      }
      if(!is.null(unlist(producto3))){
        parsed_command <- paste(parsed_command,arit2,producto3)
        parsed_title <- paste(parsed_title,arit2,"<br>",
                              names(catalogo_prod)[match(unlist(producto3),catalogo_prod)])
      }
      if(cifra == "num_total"){
        select_proddata <- pivoted_num[c(unlist(producto1),unlist(producto2),unlist(producto3))]
        select_proddata <- with(select_proddata,{
          eval(parse(text=parsed_command))
        })
      }else if(cifra == "sal_prod"){
        select_proddata <- pivoted_sal[c(unlist(producto1),unlist(producto2),unlist(producto3))]
        select_proddata <- with(select_proddata,{
          eval(parse(text=parsed_command))
        })
        
      }
      
      cifra_final <- select_proddata[[as.character(banco_principal)]]
      names(cifra_final) <- rownames(select_proddata)
      pos_bancs <- apply(select_proddata,1,FUN = order, decreasing = TRUE)
      ordered_bancs <- apply(pos_bancs,1,function(x) names(catalogo_banc[match(colnames(select_proddata)[x],catalogo_banc)]))
      
      top_desglose <- t(apply(select_proddata,1,FUN = sort, decreasing = TRUE))
      
      if(!is.null(unlist(date_target))){
        mes_target <- format(as.Date(date_target,format="%Y-%m-%d"),"%m")
        anio_target <- format(as.Date(date_target,format="%Y-%m-%d"),"%Y")
        target_data <- get_from_db_by_name_year_month_type(paste0("BM_Operativa_",anio_target,mes_target),as.numeric(anio_target),as.numeric(mes_target),1)
        
        valid_prod <- target_data$catalogo_prod[intersect(names(target_data$catalogo_prod),names(ope_data$catalogo_prod))]
        valid_prod <- valid_prod[!is.na(valid_prod)]
        equiv_prod <- function(og_prod){
          target_data$catalogo_prod[names(ope_data$catalogo_prod)[match(og_prod, ope_data$catalogo_prod)]]
        }
        equiv_banc <- function(og_banc){
          target_data$catalogo_banc[names(ope_data$catalogo_banc)[match(og_banc, ope_data$catalogo_banc)]]
        }
        valid_banc <- target_data$catalogo_banc[intersect(names(target_data$catalogo_banc),names(ope_data$catalogo_banc))]
        selecc_prod <- c(producto1,unlist(producto2),unlist(producto3))
        if(length(intersect(selecc_prod,valid_prod)) != length(selecc_prod) | length(intersect(banco_principal,valid_banc))==0 ){
          cifra_final <- NULL
          return(list(graph_mex_dist(cifra_final,list(producto = NULL, desglose = NULL)),
                      df_to_list(cifra_final)))
        }else{
          equiv_parsed_command <- target_data$catalogo_prod[names(ope_data$catalogo_prod)[match(producto1,ope_data$catalogo_prod)]]
          if(cifra == "num_total"){
            t_select_proddata <- target_data$pivoted_num[equiv_prod(unlist(producto1))]
            t_select_proddata <- with(t_select_proddata,{
              eval(parse(text=equiv_prod(parsed_command)))
            })
          }else if(cifra == "sal_prod"){
            t_select_proddata <- target_data$pivoted_sal[equiv_prod(unlist(producto1))]
            t_select_proddata <- with(t_select_proddata,{
              eval(parse(text=equiv_prod(parsed_command)))
            })
          }
          t_cifra_final <- t_select_proddata[[as.character(banco_principal)]]
          names(t_cifra_final) <- rownames(t_select_proddata)
          
          cifra_difer <- cifra_final - t_cifra_final
          t_top_desglose <- t_select_proddata
          for(i in 1:dim(t_top_desglose)[1]){
            t_top_desglose[i,1:5] <- t_top_desglose[i,pos_bancs[i,1:5]]
          }
          diff_desglose <- top_desglose - t_top_desglose
          top_desglose <- matrix(paste(as.matrix(ordered_bancs)[,1:5],as.matrix(diff_desglose)[,1:5], sep=":"), 
                                 nrow=nrow(diff_desglose), dimnames=dimnames(ordered_bancs[,1:5]))
          top_desglose <- apply(top_desglose,MARGIN = 1,function(y) paste(y,collapse = "<br>")) 
          
          
          cifra_difer <- data.frame(AGEE = catalogo_AGEE$AGEE,
                                    VALOR = cifra_difer,
                                    NOMBRE = catalogo_AGEE$dl_estado)
          cifra_difer <- cifra_difer[!is.infinite(cifra_difer$VALOR)&!is.nan(cifra_difer$VALOR),]
          
          return(list(graph_mex_dist(cifra_difer,list(producto = parsed_title, desglose = top_desglose)),
                      df_to_list(cifra_difer)))
        }
      }
      
      top_desglose <- matrix(paste(as.matrix(ordered_bancs),as.matrix(top_desglose), sep=":"), 
                             nrow=nrow(top_desglose), dimnames=dimnames(top_desglose))[,1:5]
      top_desglose <- apply(top_desglose,MARGIN = 1,function(y) paste(y,collapse = "<br>"))
      
      cifra_final <- data.frame(AGEE = names(cifra_final),
                                VALOR = cifra_final,
                                NOMBRE = catalogo_AGEE$dl_estado[match(names(cifra_final),catalogo_AGEE$AGEE)])
      cifra_final <- cifra_final[!is.infinite(cifra_final$VALOR)&!is.nan(cifra_final$VALOR),]
      return(list(graph_mex_dist(cifra_final,list(producto = parsed_title, desglose = top_desglose)),
                  df_to_list(cifra_final)))
      
    })
  }
)