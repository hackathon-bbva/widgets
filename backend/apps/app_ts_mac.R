library(dashHtmlComponents)
library(dashCoreComponents)
library(plotly)

load("data/ready_data_ts.Rda")

opts_mac <- with(unique(info_ts$meta[c("grupo","grupoDescrip")]),{
  lapply(1:length(levels(grupo)),
         function(i) list(label=grupoDescrip[i],
                          value=grupo[i]))
}
)

app_ts_mac_layout <-  htmlDiv(children = list(
  htmlLabel(id = "lab1",  "Grupo de cifras"),
  dccDropdown(id = "ts_mac_drop",
              options = opts_mac,
              clearable = FALSE,
              value = levels(info_ts$meta$grupo)[1]
  ),
  dccGraph(id = "ts_mac_graph"
  ),
  htmlButton(
    "Preparar descarga",
    id = "butt_desc"
  ),
  htmlBr(),
  htmlA(
    "Descargar",
    id = "enlace_desc_ts_mac",
    download = "rawdata.csv",
    target = "_blank",
    hidden = TRUE
  )
)
)


# App callback ####

app$callback(
  output = output(id = "ts_mac_graph", property = "figure"),
  params = list(
    input(id = "ts_mac_drop", property = "value")
  ),
  
  function(group_ind_mac){
    working_data <- info_ts[[group_ind_mac]]
    ynames <- setdiff(colnames(working_data),"Fecha")
    fig <- plot_ly() %>% 
      layout(
        xaxis = ts_xaxis,
        yaxis = list(title = info_ts$meta$grupoDescrip[info_ts$meta$grupo == group_ind_mac][1],
                     fixedrange=FALSE),
        legend = list(font = list(size=8),
                      orientation = "h",
                      xanchor = "center",
                      x = 0.5, y = 1.2),
        title = "Serie historica"
      )
    for(i in ynames){
      trace_name <- gsub("(([A-Za-z1-9.,']+\\s){8})","\\1<br> ", info_ts$meta$titulo[match(i,info_ts$meta$idSerie)])
      fig <- fig %>% add_trace(x = working_data[["Fecha"]], y = working_data[[i]],
                               name = trace_name,
                               type = "scatter",
                               mode = "line"
      )
    }
    return(fig)
  }
)

app$callback(
  output = output(id = "enlace_desc_ts_mac", property = "href"),
  params = list(
    input(id = "butt_desc", property = "n_clicks"),
    state(id = "ts_mac_graph", property = "relayoutData"),
    state(id = "ts_mac_drop", property = "value")
  ),
  function(nclicks,rl_data,group_ind_mac){
    if(is.null(nclicks)){
      return(dashNoUpdate())
    }
    idx_range <- which(rl_data$xaxis.range[[1]] < info_ts[[group_ind_mac]]$Fecha & info_ts[[group_ind_mac]]$Fecha <= rl_data$xaxis.range[[2]])
    if(rl_data$autosize == TRUE){
      idx_range <- c(1,length(info_ts[[group_ind_mac]]$Fecha))
    }
    obj_data <- info_ts[[group_ind_mac]][idx_range[1]:idx_range[length(idx_range)],]
    temp_conn <- textConnection("temp_str", "w",local = TRUE) 
    textConnectionValue(temp_conn) 
    write.table(as.matrix.data.frame(obj_data), temp_conn,sep = ",",row.names = FALSE,fileEncoding = "utf-8", eol = "\n") 
    encodedURL <- URLencode(paste(temp_str,collapse = "\n"))
    encodedURL <- paste0("data:text/csv;charset=utf-8,%EF%BB%BF",encodedURL)
    return(encodedURL)
  }
)

app$callback(
  output = output(id = "enlace_desc_ts_mac", property = "hidden"),
  params = list(
    input(id = "butt_desc", property = "n_clicks")
  ),
  function(nclicks){
    return(FALSE)
  }
)

#app$callback(
#  output = output(id = "enlace_desc_ts_mac", property = "hidden"),
#  params = list(
#    input(id = "enlace_desc_ts_mac", property = "n_clicks")
#  ),
#  function(nclicks){
#    return("hidden")
#  }
#)